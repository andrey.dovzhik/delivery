﻿using DeliveryApp.Core.Domain.CourierAggregate;
using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateCourier
{
    /// <summary>
    /// Создать курьера
    /// </summary>
    public class Command : IRequest<bool>
    {
        public string CourierName { get; }

        public Transport Transport  { get; }

        private Command()
        { }

        public Command(string courierName , string transportName)
        {
            if (string.IsNullOrWhiteSpace(courierName)) throw new ArgumentException("Некорректное значение!", nameof(courierName));
            if (string.IsNullOrWhiteSpace(transportName)) throw new ArgumentException("Некорректное значение!", 
                nameof(transportName));
            if (Transport.List().All(t => t.Name != transportName)) throw new Exception("Неизвестный транспорт!");

            CourierName = courierName;
            Transport = Transport.FromName(transportName);
        }
    }
}