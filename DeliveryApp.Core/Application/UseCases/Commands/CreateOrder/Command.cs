﻿using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder
{
    /// <summary>
    /// Создать заказ
    /// </summary>
    public class Command : IRequest<bool>
    {
        public Guid BasketId { get; }

        public string Address { get; }

        public int Weight { get;  }

        private Command()
        { }

        public Command(Guid basketId, string address, int weight)
        {
            if (basketId == Guid.Empty) throw new ArgumentException("Некорректное значение", nameof(basketId));
            if (string.IsNullOrWhiteSpace(address)) throw new ArgumentException("Некорректное значение", 
                nameof(address));
            if (weight <= 0) throw new ArgumentException("Некорректное значение", nameof(weight));

            BasketId = basketId;
            Address = address;
            Weight = weight;
        }
    }
}