﻿using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using DeliveryApp.Core.Ports;
using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder
{
    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IOrderRepository _orderRepository;
        private readonly IGeoClient _geoClient;

        public Handler(IUnitOfWork unitOfWork, IOrderRepository orderRepository, IGeoClient geoClient)
        {
            _unitOfWork = unitOfWork ?? throw new ArgumentNullException(nameof(unitOfWork));
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _geoClient = geoClient ?? throw new ArgumentNullException(nameof(geoClient));
        }

        public async Task<bool> Handle(Command message, CancellationToken cancellationToken)
        {
            var location = await _geoClient.GetGeolocationAsync(message.Address, cancellationToken);
            var weight = new Weight(message.Weight);
            var order = new Order(message.BasketId, location, weight);
            _orderRepository.Add(order);
            
            return await _unitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}