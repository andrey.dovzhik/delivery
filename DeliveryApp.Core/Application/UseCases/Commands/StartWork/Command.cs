﻿using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.StartWork
{
    /// <summary>
    /// Начать работу
    /// </summary>
    public class Command : IRequest<bool>
    {
        public Guid CourierId { get; private set; }

        private Command()
        { }
        
        public Command(Guid courierId)
        {
            if (courierId == Guid.Empty) throw new ArgumentException("Некорректное значение", nameof(courierId));
            CourierId = courierId;
        }
    }
}