﻿using Dapper;
using DeliveryApp.Core.Domain.OrderAggregate;
using MediatR;
using Npgsql;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetActiveOrders
{
    public class Handler : IRequestHandler<Query, Response>
    {
        private readonly string _connectionString;

        public Handler(string connectionString)
        {
            _connectionString = string.IsNullOrWhiteSpace(connectionString)
                ? throw new ArgumentNullException(nameof(connectionString))
                : connectionString;
        }

        public async Task<Response> Handle(Query message, CancellationToken cancellationToken)
        {
            await using var connection = new NpgsqlConnection(_connectionString);
            await connection.OpenAsync(cancellationToken);

            var result = await connection.QueryAsync<dynamic>(@"
                SELECT id, courier_id, location_x, location_y, weight, status 
                FROM public.orders 
                WHERE status != @status;"
                , new {status = Status.Completed.Value});

            var enumerable = result.ToList();
            if (enumerable.Count == 0) return null;

            var orders = new List<Order>();
            
            foreach (var item in enumerable)
            {
                orders.Add(MapToOrder(item));
            }

            return new Response(orders);
        }

        private Order MapToOrder(dynamic result)
        {
            var location = new Location { X = result.location_x, Y = result.location_y };
            var order = new Order { Id = result.id, Location = location} ;
            return order;
        }
    }
}