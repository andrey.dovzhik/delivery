using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;

namespace DeliveryApp.Core.DomainServices
{
    public class DispatchService : IDispatchService
    {
        private record Score(Courier Courier, double TimeToLocation);
        
        public Courier Dispatch(Order order, List<Courier> couriers)
        {
            if (order == null) throw new Exception("Заказ не может быть пустым!");
            if (couriers == null || couriers.Count == 0) throw new Exception("Список курьеров не может быть пустым!");

            var scores = new List<Score>();
            
            foreach (var courier in couriers.Where(c => c.Transport.CanDelivery(order.Weight)))
            {
                var timeToPoint = courier.CalculateTimeToPoint(order.Location);
                scores.Add(new Score(courier, timeToPoint));
            }

            if (scores.Count == 0) throw new Exception("Нет курьеров, которые могут перевсти заказ!");

            var minScore = scores.MinBy(c => c.TimeToLocation);
            order.AssignToCourier(minScore.Courier);

            return minScore.Courier;
        }
    }
}