using CSharpFunctionalExtensions;

namespace DeliveryApp.Core.Domain.SharedKernel;

public class Location : ValueObject
{
    private const int Min = 1;
    private const int Max = 10;
    
    public static Location MinLocation = new Location(Min, Min); 
    public static Location MaxLocation = new Location(Max, Max);
    
    public int X { get; }
    public int Y { get; }
    
    private Location()
    {}

    public Location(int x, int y)
    {
        if (x < Min || x > Max) throw new Exception($"Значение х должно быть больше 0 и меньше 10!");
        if (y < Min || y > Max) throw new Exception($"Значение y должно быть больше 0 и меньше 10!");
        X = x;
        Y = y;
    }
    
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return X;
        yield return Y;
    }
    
    public int DistanceTo(Location targetLocation) => Math.Abs(X - targetLocation.X) + Math.Abs(Y - targetLocation.Y);
    
    /// <summary>
    /// Создать случайную координату
    /// </summary>
    public static Location Random()
    {
        var rnd = new Random();
        
        return new Location(rnd.Next(MinLocation.X, MaxLocation.X), rnd.Next(MinLocation.X, MaxLocation.Y));
    }
}