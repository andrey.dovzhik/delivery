using CSharpFunctionalExtensions;

namespace DeliveryApp.Core.Domain.SharedKernel;

public class Weight : ValueObject 
{
    public int Value { get; }
    
    private Weight()
    {}

    public Weight(int value)
    {
        if (value <= 0) throw new Exception($"Значение value должно быть больше 0!");
        Value = value;
    }
    
    public static bool operator <(Weight a, Weight b) => a.Value < b.Value;
    public static bool operator <=(Weight a, Weight b) => a.Value <= b.Value;
    public static bool operator >(Weight a, Weight b) => a.Value > b.Value;
    public static bool operator >=(Weight a, Weight b) => a.Value >= b.Value;

    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }
}