using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.CourierAggregate;

public class Transport : Entity<int>
{
    public static Transport Pedestrian = new Transport(1, nameof(Pedestrian).ToLowerInvariant(), 1, new Weight(1));
    public static Transport Bicycle = new Transport(2, nameof(Bicycle).ToLowerInvariant(), 2, new Weight(4));
    public static Transport Scooter = new Transport(3, nameof(Scooter).ToLowerInvariant(), 3, new Weight(6));
    public static Transport Car = new Transport(4, nameof(Car).ToLowerInvariant(), 4, new Weight(8));
    
    public string Name { get; }
    
    public int Speed { get; }
    
    public Weight Capacity { get; }
    
    private Transport()
    {}

    protected Transport(int id, string name, int speed, Weight capacity)
        : base(id)
    {
        if (speed <= 0)
        {
            throw new Exception("Скорость транспорта не может быть меньше 0 !");
        }
        
        if (string.IsNullOrWhiteSpace(name))
        {
            throw new Exception("Название транспорта не может быть пустым!");
        }        
        
        Name = name;
        Speed = speed;
        Capacity = capacity;
    }   
    
    public static IEnumerable<Transport> List()
    {
        yield return Pedestrian;
        yield return Bicycle;
        yield return Scooter;
        yield return Car;
    }

    public static Transport FromName(string name) =>
        List().Single(t => string.Equals(t.Name, name, StringComparison.CurrentCultureIgnoreCase));
    
    public static Transport From(int id) => List().Single(t => t.Id == id);

    /// <summary>
    /// Может ли данный транспорт перевезти определенный вес?
    /// </summary>
    /// <param name="weight">Перевозимый вес</param>
    /// <returns>Перевозка веса возможна</returns>
    public bool CanDelivery(Weight weight) => Capacity >= weight;
}