using CSharpFunctionalExtensions;

namespace DeliveryApp.Core.Domain.CourierAggregate;

public class Status : ValueObject
{
    public string Value { get; }

    public static Status NotAvailable = new Status(nameof(NotAvailable).ToLower());
    public static Status Ready = new Status(nameof(Ready).ToLower());
    public static Status Busy = new Status(nameof(Busy).ToLower());

    private Status()
    {}
    
    private Status(string value)
    {
        if (string.IsNullOrWhiteSpace(value)) throw new Exception("Статус курьера должен быть непустой строкой!");
        
        Value = value;
    }

    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }
    
    public static IEnumerable<Status> List()
    {
        yield return NotAvailable;
        yield return Ready;
        yield return Busy;
    }

    public static Status FromName(string name)
    {
        var status = List().SingleOrDefault(s => string.Equals(s.Value, name, StringComparison.CurrentCultureIgnoreCase));
        
        if (status == null) throw new Exception("Некорректное имя статуса курьера!");
        
        return status;        
    }
}