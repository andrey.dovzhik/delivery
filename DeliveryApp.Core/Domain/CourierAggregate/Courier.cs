using DeliveryApp.Core.Domain.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.CourierAggregate;

public class Courier : Aggregate
{
    public string Name { get; }
    
    public Transport Transport { get; }
    
    public Location Location { get; private set; }
    
    public Status Status { get; private set; }
    
    private Courier()
    {}
    
    public Courier(string name, Transport transport)
        : base(Guid.NewGuid())
    {
        if (string.IsNullOrWhiteSpace(name))
        {
            throw new Exception("Имя курьера не может быть пустым!");
        }

        Name = name;
        Transport = transport ?? throw new Exception("У курьера должен быть транспорт!");
        Location = Location.MinLocation;
        Status = Status.NotAvailable;
    }

    public void Move(Location targetLocation)
    {
        if (Location == targetLocation) return;
        
        var cruisingRange = Transport.Speed; //запас хода
        
        var newX = Location.X;
        var newY = Location.Y;

        if (newX != targetLocation.X)
        {
            newX = Math.Min(Location.X + cruisingRange, targetLocation.X);
            var traveledX = targetLocation.X - Location.X; // сколько прошли по X
            cruisingRange -= traveledX;
        }
        
        // если ещё остался запас хода и курьер не в точке Y
        if (newY != targetLocation.Y && cruisingRange > 0)
        {
            newY = Math.Min(Location.Y + cruisingRange, targetLocation.Y);
        }

        var reachedLocation = new Location(newX, newY);

        // Если курьер выполнял заказ, то он становится свободным 
        if (Status == Status.Busy && reachedLocation == targetLocation)
        {
            Status = Status.Ready;
        }

        Location = reachedLocation;
    }

    /// <summary>
    /// Количество шагов (вызовов Move()), что бы достигнуть локации из текущей позиции курьера
    /// </summary>
    /// <param name="finalLocation">локация, которую нужно достигнуть</param>
    /// <returns>Количество шагов</returns>
    public int CalculateTimeToPoint(Location finalLocation)
    {
        var distance = Location.DistanceTo(finalLocation);

        return distance / Transport.Speed + (distance % Transport.Speed > 0 ? 1 : 0);
    }

    public void StartWork()
    {
        if (Status == Status.Busy)
            throw new Exception("Курьер уже выполняет заказ!");
            
        Status = Status.Ready;            
    }

    public void StopWork()
    {
        if (Status == Status.Busy)
            throw new Exception("Курьер ещё выполняет заказ!");        
        
        Status = Status.NotAvailable;
    }

    public void InWork()
    {
        if (Status == Status.Busy)
            throw new Exception("Курьер уже выполняет заказ!");     
        
        if (Status == Status.NotAvailable)
            throw new Exception("Курьер недоступен для назначения заказа!");          
       
        Status = Status.Busy;
    }
    
    public void CompleteWork()
    {
        if (Status != Status.Busy)
            throw new Exception("Курьер должен выполнять заказ, для его завершения!");        
        
        Status = Status.Ready;
    }    
}