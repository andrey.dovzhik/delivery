using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Domain.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.OrderAggregate;

public class Order : Aggregate
{
    public Guid? CourierId { get; private set; } 
    
    public Status Status { get; private set; }
    
    public Location Location { get; }
    
    public Weight Weight { get; }
    
    private Order()
    {}

    public Order(Guid orderId, Location location, Weight weight)
        : base(orderId)
    {

        Location = location;
        Weight = weight;
        Status = Status.Created;
        RaiseDomainEvent(new OrderCreatedDomainEvent(orderId, Status.Value));
    }

    public void Complete()
    {
        if (Status != Status.Assigned)
            throw new Exception($"Можно завершать только переданные заказы курьеру!");

        Status = Status.Completed;
        
        RaiseDomainEvent(new OrderCompletedDomainEvent(Id, Status.Value));
    }

    public void AssignToCourier(Courier courier)
    {
        if (Status != Status.Created)
            throw new Exception($"Для назначения заказа на курьера заказ должен быть в статусе {Status.Created}"!);
        
        if (courier == null)
            throw new Exception($"Курьер не назначен!");        

        CourierId = courier.Id;
        courier.InWork();
        Status = Status.Assigned;
        
        RaiseDomainEvent(new OrderAssignedDomainEvent(Id, Status.Value));
    }
}