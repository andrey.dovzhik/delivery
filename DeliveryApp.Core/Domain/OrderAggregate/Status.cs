using CSharpFunctionalExtensions;

namespace DeliveryApp.Core.Domain.OrderAggregate;

public class Status : ValueObject
{
    public string Value { get; }

    public static Status Created = new Status(nameof(Created).ToLower());
    public static Status Assigned = new Status(nameof(Assigned).ToLower());
    public static Status Completed = new Status(nameof(Completed).ToLower());
    
    private Status()
    {}

    private Status(string value)
    {
        if (string.IsNullOrWhiteSpace(value)) throw new Exception("Статус заказа должен быть непустой строкой!");
        
        Value = value;
    }

    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }

    public static IEnumerable<Status> List()
    {
        yield return Created;
        yield return Assigned;
        yield return Completed;
    }
    
    public static Status FromName(string name)
    {
        var status = List().SingleOrDefault(s => string.Equals(s.Value, name, StringComparison.CurrentCultureIgnoreCase));
        
        if (status == null) throw new Exception("Некорректное имя статуса заказа!");
        
        return status;        
    }    
}