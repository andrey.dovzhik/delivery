﻿using DeliveryApp.Core.Domain.OrderAggregate;
using Primitives;

namespace DeliveryApp.Core.Ports
{
    public interface IOrderRepository : IRepository<Order>
    {
        void Add(Order order);

        void Update(Order order);

        Order Get(Guid orderId);

        IEnumerable<Order> GetAllNotAssigned();

        IEnumerable<Order> GetAllAssigned();
    }
}