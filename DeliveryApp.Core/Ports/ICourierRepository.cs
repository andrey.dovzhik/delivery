﻿using DeliveryApp.Core.Domain.CourierAggregate;
using Primitives;

namespace DeliveryApp.Core.Ports
{
    public interface ICourierRepository : IRepository<Courier>
    {
        Courier Add(Courier courier);

        void Update(Courier courier);

        Courier Get(Guid courierId);

        IEnumerable<Courier> GetAllReady();
    }
}