using Confluent.Kafka;
using DeliveryApp.Core.Domain.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Ports;
using Newtonsoft.Json;
using OrderStatusChanged;
using Primitives.Extensions;

namespace DeliveryApp.Infrastructure.Adapters.Kafka.OrderStatusChanged;

public sealed class Producer : IBusProducer
{
    private readonly ProducerConfig _config;
    private readonly string _topic = "order.status.changed";

    public Producer(string messageBrokerHost)
    {
        if (string.IsNullOrWhiteSpace(messageBrokerHost)) throw new ArgumentException(nameof(messageBrokerHost));

        _config = new ProducerConfig
        {
            BootstrapServers = messageBrokerHost
        };
    }

    public async Task PublishOrderCreatedDomainEvent(OrderCreatedDomainEvent notification,
        CancellationToken cancellationToken)
    {
        var orderStatusChanged = new OrderStatusChangedIntegrationEvent
        {
            OrderId = notification.OrderId.ToString(),
            OrderStatus = notification.Status.ToEnum<OrderStatus>()
        };

        var message = new Message<string, string>
        {
            Key = notification.EventId.ToString(),
            Value = JsonConvert.SerializeObject(orderStatusChanged)
        };

        using var producer = new ProducerBuilder<string, string>(_config).Build();
        await producer.ProduceAsync(_topic, message, cancellationToken);
    }

    public async Task PublishOrderAssignedDomainEvent(OrderAssignedDomainEvent notification,
        CancellationToken cancellationToken)
    {
        var orderStatusChanged = new OrderStatusChangedIntegrationEvent
        {
            OrderId = notification.OrderId.ToString(),
            OrderStatus = notification.Status.ToEnum<OrderStatus>()
        };

        var message = new Message<string, string>
        {
            Key = notification.EventId.ToString(),
            Value = JsonConvert.SerializeObject(orderStatusChanged)
        };

        using var producer = new ProducerBuilder<string, string>(_config).Build();
        await producer.ProduceAsync(_topic, message, cancellationToken);
    }

    public async Task PublishOrderCompletedDomainEvent(OrderCompletedDomainEvent notification,
        CancellationToken cancellationToken)
    {
        var orderStatusChanged = new OrderStatusChangedIntegrationEvent
        {
            OrderId = notification.OrderId.ToString(),
            OrderStatus = notification.Status.ToEnum<OrderStatus>()
        };

        var message = new Message<string, string>
        {
            Key = notification.EventId.ToString(),
            Value = JsonConvert.SerializeObject(orderStatusChanged)
        };

        using var producer = new ProducerBuilder<string, string>(_config).Build();
        await producer.ProduceAsync(_topic, message, cancellationToken);
    }
}