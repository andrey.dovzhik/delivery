using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using DeliveryApp.Core.DomainServices;
using DeliveryApp.Core.Ports;
using NSubstitute;
using Primitives;
using Xunit;

namespace DeliveryApp.UnitTests.Application;

public class AssignOrdersCommandShould
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ICourierRepository _courierRepositoryMock;
    private readonly IOrderRepository _orderRepositoryMock;

    public AssignOrdersCommandShould()
    {
        _unitOfWork = Substitute.For<IUnitOfWork>();
        _courierRepositoryMock = Substitute.For<ICourierRepository>();
        _orderRepositoryMock = Substitute.For<IOrderRepository>();
    }

    [Fact]
    public async Task AssignOrders_WithCorrectData_Success()
    {
        //Arrange
        var courier1 = new Courier("Вася", Transport.Car);
        courier1.StartWork();
        var order = new Order(Guid.NewGuid(), Location.MaxLocation,
            new Weight(2));
        _orderRepositoryMock.GetAllNotAssigned().Returns(new List<Order> { order });
        _courierRepositoryMock.GetAllReady().Returns(new List<Courier> { courier1 });
        _unitOfWork.SaveEntitiesAsync().Returns(Task.FromResult(true));       
        
        var command = new DeliveryApp.Core.Application.UseCases.Commands.AssignOrders.Command();
        var handler =
            new DeliveryApp.Core.Application.UseCases.Commands.AssignOrders.Handler(_unitOfWork, _orderRepositoryMock,
                _courierRepositoryMock, new DispatchService());
        
        // Act
        var result = await handler.Handle(command, new CancellationToken());
        
        //Assert
        Assert.True(result);
    }
}