using System;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using DeliveryApp.Core.DomainServices;
using Xunit;

namespace DeliveryApp.UnitTests.DomainServices;

public class DispatchServiceTests
{
    [Fact]
    public void Dispatch_ProperAssigning_Success()
    {
        var service = new DispatchService();

        var bestCourier = new Courier("Вася", Transport.Car);
        var poorCourier1 = new Courier("Петя", Transport.Scooter);
        var poorCourier2 = new Courier("Петя", Transport.Bicycle);
        var couriers = new List<Courier> {poorCourier1, bestCourier, poorCourier2};
        var order = new Order(Guid.NewGuid(), Location.MaxLocation, new Weight(2));
        bestCourier.StartWork();
        poorCourier1.StartWork();
        poorCourier2.StartWork();

        var dispatchedCourier = service.Dispatch(order, couriers);

        Assert.Equal(bestCourier, dispatchedCourier);
    }
    [Fact]
    public void Dispatch_ProperAssigningBigWeight_Exeption()
    {
        var service = new DispatchService();
        var exceptionType = typeof(Exception);
        const string expectedMessage = "Нет курьеров, которые могут перевсти заказ!";
        var courier = new Courier("Вася", Transport.Pedestrian);
        var couriers = new List<Courier> {courier};
        var order = new Order(Guid.NewGuid(), Location.MaxLocation, new Weight(2));
        courier.StartWork();

        var ex = Assert.Throws<Exception>(() => {
            service.Dispatch(order, couriers);
        });

        Assert.Equal(expectedMessage, ex.Message);
        Assert.IsType(exceptionType, ex);
    }
    
}