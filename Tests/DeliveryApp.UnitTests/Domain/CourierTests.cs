using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using Xunit;

namespace DeliveryApp.UnitTests.Domain;

public class CourierTests
{
    [Fact]
    public void CreateCourier_CorrectState_Success()
    {
        var properLocation = new Location(1, 1);
        var properStatus = Status.NotAvailable;
        
        var courier = new Courier("Вася", Transport.Bicycle);

        Assert.Equal(properLocation, courier.Location);        
        Assert.Equal(properStatus, Status.NotAvailable);        
    }
    
    [Theory]
    [InlineData("pedestrian", 2,2, 2, 1)]
    [InlineData("bicycle", 2,2, 2, 2)]
    [InlineData("car", 2,2, 2, 2)]
    [InlineData("car", 4,1, 4, 1)]
    [InlineData("car", 6,1, 5, 1)]
    [InlineData("car", 1,4, 1, 4)]
    [InlineData("car", 1,6, 1, 5)]
    [InlineData("car", 4,4, 4, 2)]
    public void Move_CorrectLocation_Success(string transport, int targetX, int targetY, int newCourierX, int newCourierY)
    {
        var courier = new Courier("Вася", Transport.FromName(transport));
        var targetLocation = new Location(targetX, targetY);
        var correctCourierLocation = new Location(newCourierX, newCourierY);
        
        courier.Move(targetLocation);

        Assert.Equal(correctCourierLocation, courier.Location);
    }

    [Theory]
    [InlineData("pedestrian", 2,1, 1)]    
    [InlineData("pedestrian", 2,2, 2)]    
    [InlineData("bicycle", 4,1, 2)]    
    [InlineData("bicycle", 5,5, 4)]    
    public void CalculateTimeToPoint_Calculation_Success(string transport, int targetX, int targetY, int stepsCount)
    {
        var courier = new Courier("Вася", Transport.FromName(transport));
        var targetLocation = new Location(targetX, targetY);
        
        var calculatedSteps = courier.CalculateTimeToPoint(targetLocation);

        Assert.Equal(stepsCount, calculatedSteps);        
    }

    [Fact]
    public void StartWork_ChangeState_Ready()
    {
        var courier = new Courier("Вася", Transport.FromName("pedestrian"));
        
        courier.StartWork();
        
        Assert.Equal(Status.Ready, courier.Status);
    }
}