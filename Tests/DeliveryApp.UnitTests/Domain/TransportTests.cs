using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using Xunit;

namespace DeliveryApp.UnitTests.Domain;

public class TransportTests
{
    [Fact]
    public void CanDelivery_CorrectWight_Success()
    {
        var transport = Transport.Bicycle;

        var can = transport.CanDelivery(new Weight(2));
        
        Assert.True(can);
    }
    
    [Fact]
    public void CanDelivery_WrongWight_Fail()
    {
        var transport = Transport.Bicycle;

        var can = transport.CanDelivery(new Weight(20));
        
        Assert.False(can);
    }
}