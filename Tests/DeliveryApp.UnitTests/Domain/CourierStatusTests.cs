using System;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.CourierAggregate;
using Xunit;

namespace DeliveryApp.UnitTests.Domain;

public class CourierStatusTests
{
    public static IEnumerable<object[]> GetStatuses()
    {
        yield return [Status.NotAvailable, "notavailable"];
        yield return [Status.Ready, "ready"];
        yield return [Status.Busy, "busy"];
    }
    
    [Theory]
    [MemberData(nameof(GetStatuses))]
    public void Status_HasCorrectNames(Status status, string name)
    {
        Assert.Equal(status.Value, name);
    }
    
    [Theory]
    [MemberData(nameof(GetStatuses))]
    public void FromName_CorrectValues_Success(Status status, string name)
    {
        var parsedStatus = Status.FromName(name);

        Assert.Equal(status.Value, parsedStatus.Value);
    }    
    
    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("dummy")]
    public void FromName_ParsingIncorrectValue_Fail(string invalidName)
    {
        var exceptionType = typeof(Exception);
        const string expectedMessage = "Некорректное имя статуса курьера!";        
        
        var ex = Assert.Throws<Exception>(() => {
            Status.FromName(invalidName);
        });

        Assert.Equal(expectedMessage, ex.Message);
        Assert.IsType(exceptionType, ex);        
    }
}