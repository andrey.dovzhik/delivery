using System;
using DeliveryApp.Core.Domain.SharedKernel;
using Xunit;

namespace DeliveryApp.UnitTests.Domain;

public class WeightTests
{
    [Fact]
    public void Create_ValidData_Success()
    {
        const int correctValue = 5;
            
        var weight = new Weight(correctValue);
        
        Assert.Equal(correctValue, weight.Value);
    }
    
    [Theory]
    [InlineData(0)]
    [InlineData(-5)]
    public void Create_InvalidData_Exception(int incorrectValue)
    {
        var exceptionType = typeof(Exception);
        const string expectedMessage = "Значение value должно быть больше 0!";
        
        var ex = Assert.Throws<Exception>(() => {
            var w = new Weight(incorrectValue);
        });

        Assert.Equal(expectedMessage, ex.Message);
        Assert.IsType(exceptionType, ex);
    }

    [Fact]
    public void Compare_HeavierWeight_Success()
    {
        var heavier = new Weight(10);
        var lighter = new Weight(5);

        var result = heavier > lighter;
        
        Assert.True(result);
    }
    
    [Fact]
    public void Compare_LighterWeight_Success()
    {
        var heavier = new Weight(10);
        var lighter = new Weight(5);

        var result = lighter < heavier;
        
        Assert.True(result);
    }
        
    [Fact]
    public void Compare_EquivalentWeight_Success()
    {
        var weight1 = new Weight(10);
        var weight2 = new Weight(10);

        var result = weight1 == weight2;
        var result2 = weight1.Equals(weight2);
        
        Assert.True(result);
        Assert.True(result2);
    }
}