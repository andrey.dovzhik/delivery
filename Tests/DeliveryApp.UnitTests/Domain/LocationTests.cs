using System;
using DeliveryApp.Core.Domain.SharedKernel;
using Xunit;

namespace DeliveryApp.UnitTests.Domain;

public class LocationTests
{
    [Theory]
    [InlineData(1,1)]
    [InlineData(10,10)]
    public void Create_ValidData_Success(int correctX, int correctY)
    {
        var location = new Location(correctX, correctY);
        
        Assert.Equal(correctX, location.X);
        Assert.Equal(correctY, location.Y);
    }
    
    [Theory]
    [InlineData(11)]
    [InlineData(0)]
    [InlineData(-5)]
    public void Create_InvalidDataX_Exception(int incorrectX)
    {
        const int correctY = 5;
        const string expectedMessage = "Значение х должно быть больше 0 и меньше 10!";
        var exceptionType = typeof(Exception);
        
        var ex = Assert.Throws<Exception>(() => {
            var loc = new Location(incorrectX, correctY);
        });

        Assert.Equal(expectedMessage, ex.Message);
        Assert.IsType(exceptionType, ex);
    }
    
    [Theory]
    [InlineData(11)]
    [InlineData(0)]
    [InlineData(-5)]
    public void Create_InvalidDataY_Exception(int incorrectY)
    {
        const int correctX = 5;
        const string expectedMessage = "Значение y должно быть больше 0 и меньше 10!";
        var exceptionType = typeof(Exception);
        
        var ex = Assert.Throws<Exception>(() => {
            var loc = new Location(correctX, incorrectY);
        });

        Assert.Equal(expectedMessage, ex.Message);
        Assert.IsType(exceptionType, ex);
    }
    
    [Fact]
    public void Compare_EquivalentLocation_Success()
    {
        var location1 = new Location(5,6);
        var location2 = new Location(5,6);

        var result = location1 == location2;
        var result2 = location1.Equals(location2);
        
        Assert.True(result);
        Assert.True(result2);
    }

    [Theory]
    [InlineData(1,1, 2, 2, 2)]
    [InlineData(10,10, 1, 1, 18)]
    [InlineData(1,1, 1, 1, 0)]
    public void DistanceTo_Calculation_Success(int x1, int y1, int x2, int y2, int correctDistance)
    {
        var location1 = new Location(x1,y1);
        var location2 = new Location(x2,y2);

        var calculatedRange = location1.DistanceTo(location2);

        Assert.Equal(correctDistance, calculatedRange);
    }
}