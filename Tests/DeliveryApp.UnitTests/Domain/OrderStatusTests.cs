using System;
using System.Collections.Generic;
using DeliveryApp.Core.Domain.OrderAggregate;
using Xunit;

namespace DeliveryApp.UnitTests.Domain;

public class OrderStatusTests
{
    public static IEnumerable<object[]> GetStatuses()
    {
        yield return [Status.Created, "created"];
        yield return [Status.Assigned, "assigned"];
        yield return [Status.Completed, "completed"];
    }
    
    [Theory]
    [MemberData(nameof(GetStatuses))]
    public void Status_HasCorrectNames(Status status, string name)
    {
        Assert.Equal(status.Value, name);
    }
    
    [Theory]
    [MemberData(nameof(GetStatuses))]
    public void FromName_CorrectValues_Success(Status status, string name)
    {
        var parsedStatus = Status.FromName(name);

        Assert.Equal(status.Value, parsedStatus.Value);
    }    
    
    [Theory]
    [InlineData("")]
    [InlineData(" ")]
    [InlineData("dummy")]
    public void FromName_ParsingIncorrectValue_Fail(string invalidName)
    {
        var exceptionType = typeof(Exception);
        const string expectedMessage = "Некорректное имя статуса заказа!";        
        
        var ex = Assert.Throws<Exception>(() => {
            Status.FromName(invalidName);
        });

        Assert.Equal(expectedMessage, ex.Message);
        Assert.IsType(exceptionType, ex);        
    }    
}