using System;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using Xunit;
using Status = DeliveryApp.Core.Domain.OrderAggregate.Status;

namespace DeliveryApp.UnitTests.Domain;

public class OrderTests
{
    [Fact]
    public void Create_State_Correct()
    {
        var order = new Order(Guid.NewGuid(), Location.MaxLocation, new Weight(10));
        
        Assert.Equal(Status.Created, order.Status);
        Assert.Null(order.CourierId);
    }
    
    [Fact]
    public void AssignToCourier_State_Correct()
    {
        var order = new Order(Guid.NewGuid(), Location.MaxLocation, new Weight(10));
        var courier = new Courier("Вася", Transport.Bicycle);
        
        courier.StartWork();
        order.AssignToCourier(courier);
        
        Assert.Equal(Status.Assigned, order.Status);
        Assert.NotNull(order.CourierId);
    }
}