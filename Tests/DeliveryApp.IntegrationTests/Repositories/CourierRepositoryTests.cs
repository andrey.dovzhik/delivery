using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories
{
    public class CourierRepositoryShould : IAsyncLifetime
    {
        private ApplicationDbContext _context;
        private readonly IMediator _mediator;

        /// <summary>
        /// Настройка Postgres из библиотеки TestContainers
        /// </summary>
        /// <remarks>По сути это Docker контейнер с Postgres</remarks>
        private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
            .WithImage("postgres:14.7")
            .WithDatabase("order")
            .WithUsername("username")
            .WithPassword("secret")
            .WithCleanUp(true)
            .Build();

        /// <summary>
        /// Ctr
        /// </summary>
        /// <remarks>Вызывается один раз перед всеми тестами в рамках этого класса</remarks>
        public CourierRepositoryShould()
        {
            _mediator = Substitute.For<IMediator>();
        }

        /// <summary>
        /// Инициализируем окружение
        /// </summary>
        /// <remarks>Вызывается перед каждым тестом</remarks>
        public async Task InitializeAsync()
        {
            //Стартуем БД (библиотека TestContainers запускает Docker контейнер с Postgres)
            await _postgreSqlContainer.StartAsync();

            //Накатываем миграции и справочники
            var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
                _postgreSqlContainer.GetConnectionString(),
                npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
            _context = new ApplicationDbContext(contextOptions);
            _context.Database.Migrate();
        }

        /// <summary>
        /// Уничтожаем окружение
        /// </summary>
        /// <remarks>Вызывается после каждого теста</remarks>
        public async Task DisposeAsync()
        {
            await _postgreSqlContainer.DisposeAsync().AsTask();
        }

        [Fact]
        public async Task CanAddCourier()
        {
            //Arrange
            var courier = new Courier("Иван", Transport.Pedestrian);

            //Act
            var courierRepository = new CourierRepository(_context);
            var unitOfWork = new UnitOfWork(_context, _mediator);
            courierRepository.Add(courier);
            await unitOfWork.SaveEntitiesAsync();

            //Assert
            var courierFromDb = courierRepository.Get(courier.Id);
            courier.Should().BeEquivalentTo(courierFromDb);
        }

        [Fact]
        public async Task CanUpdateCourier()
        {
            //Arrange
            var courier = new Courier("Иван", Transport.Pedestrian);

            var courierRepository = new CourierRepository(_context);
            var unitOfWork = new UnitOfWork(_context, _mediator);
            courierRepository.Add(courier);
            await unitOfWork.SaveEntitiesAsync();

            //Act
            courier.StartWork();
            courierRepository.Update(courier);
            await unitOfWork.SaveEntitiesAsync();

            //Assert
            var courierFromDb = courierRepository.Get(courier.Id);
            courier.Should().BeEquivalentTo(courierFromDb);
            courierFromDb.Status.Should().Be(Status.Ready);
        }

        [Fact]
        public async Task CanGetById()
        {
            //Arrange
            var courier = new Courier("Иван", Transport.Pedestrian);

            //Act
            var courierRepository = new CourierRepository(_context);
            var unitOfWork = new UnitOfWork(_context, _mediator);
            courierRepository.Add(courier);
            await unitOfWork.SaveEntitiesAsync();

            //Assert
            var courierFromDb = courierRepository.Get(courier.Id);
            courier.Should().BeEquivalentTo(courierFromDb);
        }

        [Fact]
        public async Task CanGetAllActive()
        {
            //Arrange
            var courier1 = new Courier("Иван", Transport.Pedestrian);
            courier1.StopWork();

            var courier2 = new Courier("Борис", Transport.Pedestrian);
            courier2.StartWork();

            var courierRepository = new CourierRepository(_context);
            var unitOfWork = new UnitOfWork(_context, _mediator);
            courierRepository.Add(courier1);
            courierRepository.Add(courier2);
            await unitOfWork.SaveEntitiesAsync();

            //Act
            var activeCouriersFromDb = courierRepository.GetAllReady();

            //Assert
            var couriersFromDb = activeCouriersFromDb.ToList();
            couriersFromDb.Should().NotBeEmpty();
            couriersFromDb.Count().Should().Be(1);
            couriersFromDb.First().Should().BeEquivalentTo(courier2);
        }
    }
}