using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NSubstitute;
using Testcontainers.PostgreSql;
using Xunit;

namespace DeliveryApp.IntegrationTests.Repositories
{
    public class OrderRepositoryShould : IAsyncLifetime
    {
        private ApplicationDbContext _context;
        private readonly IMediator _mediator;
        private Location _location;
        private Weight _weight;

        /// <summary>
        /// Настройка Postgres из библиотеки TestContainers
        /// </summary>
        /// <remarks>По сути это Docker контейнер с Postgres</remarks>
        private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
            .WithImage("postgres:14.7")
            .WithDatabase("order")
            .WithUsername("username")
            .WithPassword("secret")
            .WithCleanUp(true)
            .Build();

        /// <summary>
        /// Ctr
        /// </summary>
        /// <remarks>Вызывается один раз перед всеми тестами в рамках этого класса</remarks>
        public OrderRepositoryShould()
        {
            _mediator = Substitute.For<IMediator>();
            _weight = new Weight(2);
            _location = new Location(1, 1);
        }

        /// <summary>
        /// Инициализируем окружение
        /// </summary>
        /// <remarks>Вызывается перед каждым тестом</remarks>
        public async Task InitializeAsync()
        {
            //Стартуем БД (библиотека TestContainers запускает Docker контейнер с Postgres)
            await _postgreSqlContainer.StartAsync();

            //Накатываем миграции и справочники
            var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(
                _postgreSqlContainer.GetConnectionString(),
                npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.MigrationsAssembly("DeliveryApp.Infrastructure");
                }).Options;
            _context = new ApplicationDbContext(contextOptions);
            _context.Database.Migrate();
        }

        /// <summary>
        /// Уничтожаем окружение
        /// </summary>
        /// <remarks>Вызывается после каждого теста</remarks>
        public async Task DisposeAsync()
        {
            await _postgreSqlContainer.DisposeAsync().AsTask();
        }

        [Fact]
        public async Task CanAddOrder()
        {
            //Arrange
            var orderId = Guid.NewGuid();
            var order = new Order(orderId, _location, _weight);

            //Act
            var orderRepository = new OrderRepository(_context);
            var unitOfWork = new UnitOfWork(_context, _mediator);
            
            orderRepository.Add(order);
            await unitOfWork.SaveEntitiesAsync();

            //Assert
            var orderFromDb = orderRepository.Get(order.Id);
            order.Should().BeEquivalentTo(orderFromDb);
        }

        [Fact]
        public async Task CanUpdateOrder()
        {
            //Arrange
            var courier = new Courier("Иван", Transport.Pedestrian);
            courier.StartWork();

            var orderId = Guid.NewGuid();
            var order = new Order(orderId, _location, _weight);

            var orderRepository = new OrderRepository(_context);
            orderRepository.Add(order);
            
            var unitOfWork = new UnitOfWork(_context, _mediator);
            await unitOfWork.SaveEntitiesAsync();

            //Act
            order.AssignToCourier(courier);
            orderRepository.Update(order);
            await unitOfWork.SaveEntitiesAsync();

            //Assert
            var orderFromDb = orderRepository.Get(order.Id);
            order.Should().BeEquivalentTo(orderFromDb);
        }

        [Fact]
        public async Task CanGetById()
        {
            //Arrange
            var orderId = Guid.NewGuid();
            var order = new Order(orderId, _location, _weight);

            //Act
            var orderRepository = new OrderRepository(_context);
            orderRepository.Add(order);
            
            var unitOfWork = new UnitOfWork(_context, _mediator);
            await unitOfWork.SaveEntitiesAsync();

            //Assert
            var orderFromDb = orderRepository.Get(order.Id);
            order.Should().BeEquivalentTo(orderFromDb);
        }

        [Fact]
        public async Task CanGetAllActive()
        {
            //Arrange
            var courier = new Courier("Иван", Transport.Pedestrian);
            courier.StartWork();

            var order1Id = Guid.NewGuid();
            var order1 = new Order(order1Id, _location, _weight);
            order1.AssignToCourier(courier);

            var order2Id = Guid.NewGuid();
            var order2 = new Order(order2Id, _location, _weight);

            var orderRepository = new OrderRepository(_context);
            orderRepository.Add(order1);
            orderRepository.Add(order2);
            
            var unitOfWork = new UnitOfWork(_context, _mediator);
            await unitOfWork.SaveEntitiesAsync();

            //Act
            var activeOrdersFromDb = orderRepository.GetAllNotAssigned();

            //Assert
            var ordersFromDb = activeOrdersFromDb.ToList();
            ordersFromDb.Should().NotBeEmpty();
            ordersFromDb.Count().Should().Be(1);
            ordersFromDb.First().Should().BeEquivalentTo(order2);
        }
    }
}